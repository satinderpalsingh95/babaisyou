//
//  GameScene.swift
//  BabaIsYou-F19
//
//  Created by Parrot on 2019-10-17.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {

    var baba:SKSpriteNode!
    var stopblock:SKSpriteNode!
    var isblock:SKSpriteNode!
    var wallblock:SKSpriteNode!
    var flagblock:SKSpriteNode!
    var flag:SKSpriteNode!
    var winblock:SKSpriteNode!
    var wall:SKSpriteNode!
    var isblockupper:SKSpriteNode!
    
    
    var wallArr = [SKSpriteNode?]()
    
    

    let approx = 7
    let imagesize = -64
    
    
    
    let baba_SPEED:CGFloat = 20
    override func didMove(to view: SKView) {
        self.physicsWorld.contactDelegate = self
        
//        //initializing objects
       self.baba = self.childNode(withName: "baba") as? SKSpriteNode
        self.stopblock = self.childNode(withName: "stopblock") as? SKSpriteNode
        
        self.wallblock = self.childNode(withName: "wallblock") as? SKSpriteNode
        self.flagblock = self.childNode(withName: "flagblock") as? SKSpriteNode
         self.flag = self.childNode(withName: "flag") as? SKSpriteNode
         self.winblock = self.childNode(withName: "winblock") as? SKSpriteNode
         

        //initalizing hit box
        self.baba.physicsBody = SKPhysicsBody(rectangleOf: baba.size)
        self.baba.physicsBody?.affectedByGravity = false
        self.baba.physicsBody?.categoryBitMask = 1
        self.baba.physicsBody?.collisionBitMask = 255
        self.baba.physicsBody?.contactTestBitMask = 0
        self.baba.physicsBody?.allowsRotation = false
        
        
        

        
        self.stopblock.physicsBody = SKPhysicsBody(rectangleOf: stopblock.size)
        self.stopblock.physicsBody?.affectedByGravity = false
        self.stopblock.physicsBody?.categoryBitMask = 32
        self.stopblock.physicsBody?.collisionBitMask = 255
        self.stopblock.physicsBody?.contactTestBitMask = 126
        self.stopblock.physicsBody?.allowsRotation = false
        
        self.wallblock.physicsBody = SKPhysicsBody(rectangleOf: wallblock.size)
        self.wallblock.physicsBody?.affectedByGravity = false
        self.wallblock.physicsBody?.categoryBitMask = 16
        self.wallblock.physicsBody?.collisionBitMask = 255
        self.wallblock.physicsBody?.contactTestBitMask = 126
        self.wallblock.physicsBody?.allowsRotation = false

        

        self.flagblock.physicsBody = SKPhysicsBody(rectangleOf: flagblock.size)
        self.flagblock.physicsBody?.affectedByGravity = false
        self.flagblock.physicsBody?.categoryBitMask = 2
        self.flagblock.physicsBody?.collisionBitMask = (255)
        self.flagblock.physicsBody?.contactTestBitMask = 126
        self.flagblock.physicsBody?.allowsRotation = false
        
        
        self.flag.physicsBody = SKPhysicsBody(rectangleOf: flag.size)
        self.flag.physicsBody?.affectedByGravity = false
        self.flag.physicsBody?.categoryBitMask = 64
        self.flag.physicsBody?.collisionBitMask = 126
        self.flag.physicsBody?.contactTestBitMask = 126
        self.flag.physicsBody?.allowsRotation = false
        
        
        
        self.winblock.physicsBody = SKPhysicsBody(rectangleOf: winblock.size)
        self.winblock.physicsBody?.affectedByGravity = false
        self.winblock.physicsBody?.categoryBitMask = 8
        self.winblock.physicsBody?.collisionBitMask = 255
        self.winblock.physicsBody?.contactTestBitMask = 126
        self.winblock.physicsBody?.allowsRotation = false
        
        
        
        // setup isBlock sprites
        self.enumerateChildNodes(withName: "isblock") {
            (node, stop) in
             self.isblockupper = node as! SKSpriteNode
            self.isblockupper.physicsBody = SKPhysicsBody(rectangleOf: self.isblockupper.size)
            self.isblockupper.physicsBody?.affectedByGravity = false
            self.isblockupper.physicsBody?.categoryBitMask = 4
            self.isblockupper.physicsBody?.collisionBitMask = 0
            self.isblockupper.physicsBody?.contactTestBitMask = 126
            
        }
        // setup wall sprites
        self.enumerateChildNodes(withName: "wall") {
            (node, stop) in
             self.wall = node as! SKSpriteNode
            self.wall.physicsBody = SKPhysicsBody(rectangleOf: self.wall.size)
            self.wall.physicsBody?.affectedByGravity = false
            self.wall.physicsBody?.categoryBitMask = 128
            self.wall.physicsBody?.collisionBitMask = 0
            self.wall.physicsBody?.isDynamic = false
            
            self.wallArr.append(self.wall)
            
        }
        
        
        self.isblock = self.childNode(withName: "isblock") as? SKSpriteNode
        
        
        
        
    }
   
    func didBegin(_ contact: SKPhysicsContact) {
        print("Something collided!")
        
        if self.wallblock.frame.intersects(self.isblock.frame) {
            if self.isblock.frame.intersects(self.stopblock.frame) {
                self.baba.physicsBody?.collisionBitMask = 128
                print("wall is stop")
            }
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
     
        if( Int(wallblock.position.x) - Int(isblock.position.x) == imagesize && Int(isblock.position.x) - Int(stopblock.position.x) == imagesize && (Int(stopblock.position.y) < Int(isblock.position.y)+approx) && (Int(stopblock.position.y) > Int(isblock.position.y)-approx) && (Int(wallblock.position.y) < Int(isblock.position.y)+approx) && (Int(wallblock.position.y) > Int(isblock.position.y)-approx))
               {

                  print("wall is stop ONN")
                   self.baba.physicsBody?.collisionBitMask = 191
                self.flag.physicsBody?.collisionBitMask = 127
               }
           else
               {
                     print("wall is stop OFFF")
                    self.baba.physicsBody?.collisionBitMask = 63
                    self.flagblock.physicsBody?.collisionBitMask = 127
                    self.wallblock.physicsBody?.collisionBitMask = 127
                     self.stopblock.physicsBody?.collisionBitMask = 127
                     self.winblock.physicsBody?.collisionBitMask = 127
                
                    //if flag/wall is win wall is never stop
                
                if( Int(flagblock.position.x) - Int(isblockupper.position.x) == -64 && Int(isblockupper.position.x) - Int(winblock.position.x) == -64 &&
                            (Int(flagblock.position.y) < Int(isblockupper.position.y)+5) && (Int(flagblock.position.y) > Int(isblockupper .position.y)-5) && (Int(winblock.position.y) < Int(isblockupper.position.y)+5) && (Int(winblock.position.y) > Int(isblockupper.position.y)-5)){
                                print("Flag is WINNNNNN")
                                
                                if(self.baba.frame.intersects(self.flag.frame) == true) {
                                    print("atlast you wonnnn")
                                    
                                }
                                
                                
                            }
                if( Int(flagblock.position.x) - Int(isblock.position.x) == imagesize && Int(isblock.position.x) - Int(winblock.position.x) == imagesize &&
                (Int(flagblock.position.y) < Int(isblock.position.y)+approx) && (Int(flagblock.position.y) > Int(isblock .position.y)-approx) && (Int(winblock.position.y) < Int(isblock.position.y)+approx) && (Int(winblock.position.y) > Int(isblock.position.y)-approx)){
                    print("Flag is WINNNNNN")
                    
                    if(self.baba.frame.intersects(self.flag.frame) == true) {
                        print("atlast you wonnnn")
                        
                    }
                    
                }


                }
        
        print(Int(wallblock.position.x));
        print(Int(isblockupper.position.x))
        print(Int(winblock.position.x))
        
        if( Int(wallblock.position.x) - Int(isblockupper.position.x) == imagesize && Int(isblockupper.position.x) - Int(winblock.position.x) == imagesize && (Int(wallblock.position.y) < Int(isblockupper.position.y)+approx) && (Int(wallblock.position.y) > Int(isblockupper.position.y)-approx) && (Int(winblock.position.y) < Int(isblockupper.position.y)+approx) && (Int(winblock.position.y) > Int(isblockupper.position.y)-approx)){
            
            //print("inside wall is win")
            
            if ((self.wallArr[0]!.frame.intersects(self.baba.frame)) == true)
            {
                print("atlast you wonnnn")
            }else if ((self.wallArr[1]!.frame.intersects(self.baba.frame)) == true)
            {
                print("atlast you wonnnn")
            }else if ((self.wallArr[2]!.frame.intersects(self.baba.frame)) == true)
            {
                print("atlast you wonnnn")
            }else if ((self.wallArr[3]!.frame.intersects(self.baba.frame)) == true)
            {
                print("atlast you wonnnn")
            }else{
                print("lose")
            }
            
            
        }
        else if( Int(wallblock.position.x) - Int(isblock.position.x) == imagesize && Int(isblock.position.x) - Int(winblock.position.x) == imagesize && (Int(wallblock.position.y) < Int(isblock.position.y)+approx) && (Int(wallblock.position.y) > Int(isblock.position.y)-approx) && (Int(winblock.position.y) < Int(isblock.position.y)+approx) && (Int(winblock.position.y) > Int(isblock.position.y)-approx)){
            
            //print("inside wall is win")
            
            if ((self.wallArr[0]!.frame.intersects(self.baba.frame)) == true)
            {
                print("atlast you wonnnn")
            }else if ((self.wallArr[1]!.frame.intersects(self.baba.frame)) == true)
            {
                print("atlast you wonnnn")
            }else if ((self.wallArr[2]!.frame.intersects(self.baba.frame)) == true)
            {
                print("atlast you wonnnn")
            }else if ((self.wallArr[3]!.frame.intersects(self.baba.frame)) == true)
            {
                print("atlast you wonnnn")
            }else{
                print("lose")
            }
            
            
        }
        
        if( Int(wallblock.position.x) - Int(isblockupper.position.x) == imagesize && Int(isblockupper.position.x) - Int(stopblock.position.x) == imagesize && (Int(stopblock.position.y) < Int(isblockupper.position.y)+approx) && (Int(stopblock.position.y) > Int(isblockupper.position.y)-approx) && (Int(wallblock.position.y) < Int(isblockupper.position.y)+approx) && (Int(wallblock.position.y) > Int(isblockupper.position.y)-approx))
            {

               print("wall is stop ONN")
                self.baba.physicsBody?.collisionBitMask = 191
             self.flag.physicsBody?.collisionBitMask = 127
            }
        
        
        if( Int(flagblock.position.x) - Int(isblock.position.x) == imagesize && Int(isblock.position.x) - Int(stopblock.position.x) == imagesize && (Int(flagblock.position.y) < Int(isblock.position.y)+approx) && (Int(flagblock.position.y) > Int(isblock.position.y)-approx) && (Int(stopblock.position.y) < Int(isblock.position.y)+approx) && (Int(stopblock.position.y) > Int(isblock.position.y)-approx))
            {
                print("flag is stop ONN")
                self.flag.physicsBody?.collisionBitMask = 0
               self.baba.physicsBody?.collisionBitMask = 127
            }
        else if( Int(flagblock.position.x) - Int(isblockupper.position.x) == imagesize && Int(isblockupper.position.x) - Int(stopblock.position.x) == imagesize && (Int(flagblock.position.y) < Int(isblockupper.position.y)+approx) && (Int(flagblock.position.y) > Int(isblockupper.position.y)-approx) && (Int(stopblock.position.y) < Int(isblockupper.position.y)+approx) && (Int(stopblock.position.y) > Int(isblock.position.y)-approx))
            {
                print("flag is stop ONN")
                self.flag.physicsBody?.collisionBitMask = 0
               self.baba.physicsBody?.collisionBitMask = 127
            }
        else{
            print("flag is stop OFFFF")
            self.flag.physicsBody?.collisionBitMask = 127
            //self.baba.physicsBody?.collisionBitMask = 63
        }
        
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // GET THE POSITION WHERE THE MOUSE WAS CLICKED
        // ---------------------------------------------
        let mouseTouch = touches.first
        if (mouseTouch == nil) {
            return
        }
        let location = mouseTouch!.location(in: self)

        // WHAT NODE DID THE PLAYER TOUCH
        // ----------------------------------------------
        let nodeTouched = atPoint(location).name
        //print("Player touched: \(nodeTouched)")
        
        
        // GAME LOGIC: Move player based on touch
        if (nodeTouched == "upButton") {
            // move up
            self.baba.position.y = self.baba.position.y + baba_SPEED
        }
        else if (nodeTouched == "downButton") {
            // move down
             self.baba.position.y = self.baba.position.y - baba_SPEED
        }
        else if (nodeTouched == "leftButton") {
            // move left
             self.baba.position.x = self.baba.position.x - baba_SPEED
        }
        else if (nodeTouched == "rightButton") {
            // move right
             self.baba.position.x = self.baba.position.x + baba_SPEED
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
}
